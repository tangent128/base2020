import { KeyHandler, KeyName } from "../applet/Keyboard";

export class Buttons implements KeyHandler {

    keys: KeyName[] = [];

    public update(key: KeyName, state: "press" | "release") {
        const newKeys = this.keys.filter(k => k != key);
        if(state == "press") {
            newKeys.push(key);
            newKeys.sort();
        }
        this.keys = newKeys;
    }

    public block() {
        this.keys = [];
    }

    public getPressed(): KeyName[] {
        return this.keys.slice();
    }
}
