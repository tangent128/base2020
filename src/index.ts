import { Select } from "./applet/Init";
import { KeyName } from "./applet/Keyboard";
import { BindTests } from "./ecs/test";
import { Data } from "./game/GameComponents";
import { Main } from "./game/Main";
import { Connection } from "./net/Connection";
import {} from "./net/LoopbackServer";

/* // Hot Module Reloading stub, if that's viable
 * declare const module: any;
 * if(module.hot) {
 *     const gen = module.hot.data?.gen ?? 0;
 *     module.hot.accept();
 *     module.hot.dispose((data: any) => {
 *         data.gen = gen + 1;
 *     });
 * }
 */

Select(".GameCanvas").forEachCanvas((c, cx, keys) => {
    const connection = new Connection<KeyName[], Data>(`ws://${location.host}/base2020.ws`);
    new Main(c, cx, keys, connection.socket);
    c.focus();
});

BindTests();
