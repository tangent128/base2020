use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::time::Duration;

pub mod agent;
pub mod server;

/// There is a point at which a client falls far enough behind
/// that it's probably not worth trying to catch them up; for now,
/// implement this as a buffer size limit and disconnect a client if
/// the cap is reached. More elegant solutions may be reached in the future.
const CHANNEL_BUFFER: usize = 200;

/// Roughly 30 fps
pub static TICK_LENGTH: Duration = Duration::from_millis(33);

pub type PlayerId = usize;
pub type PlayerInput = Value;
pub type GameState = Value;

#[derive(Deserialize, Debug)]
#[serde(tag = "t")]
pub enum ClientMessage {
    #[serde(rename = "s")]
    SetState {
        #[serde(rename = "s")]
        new_state: GameState
    },
    #[serde(rename = "i")]
    Input {
        #[serde(rename = "i")]
        local_input: PlayerInput
    },
    #[serde(rename = "g")]
    GetState {
        #[serde(rename = "c")]
        cookie: usize,
        #[serde(rename = "s")]
        state: GameState
    },
}

#[derive(Serialize, Debug, Clone)]
pub struct Meta {
    pub helo: Option<String>,
    pub version: &'static str,
}

#[derive(Serialize, Debug, Clone)]
#[serde(tag = "t")]
pub enum ServerMessage {
    #[serde(rename = "m")]
    Meta {
        #[serde(flatten)]
        #[serde(rename = "m")]
        meta: Meta,
    },
    #[serde(rename = "s")]
    SetState {
        #[serde(rename = "u")]
        player_id: Option<PlayerId>,
        #[serde(rename = "s")]
        state: GameState
    },
    #[serde(rename = "i")]
    Input {
        #[serde(rename = "i")]
        total_input: Vec<PlayerInput>
    },
    #[serde(rename = "g")]
    GetState {
        #[serde(rename = "c")]
        cookie: usize
    },
}
