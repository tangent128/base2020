Open:
- Cloneable RNG that goes in state (use MurmurHash3 finalizer in counter mode?)
- remove all random() calls
- multiple "rooms" on server
- sync to op on join

Done:
- Insecured websocket server implementation
    - desynced on join
    - reset on join
- Multiplayer loopback server
- Refactor input generics to distinct local/full types
- Refactor input messages for more than one player
- Rework State implementation for easier cloning/deserialization
- Test Lockstep/rollback
- Smarter typings for Join/Lookup functions
- Parcel scripts
- DeepCopy/Equals interfaces
- Rollback state management
    - state + input reducer function
    - maintain predicted state
    - inform of local actions
    - maintain last-canonical state
    - inform of server state
- Lockstep driver loop
